# build stage
FROM node:latest as node

WORKDIR /app
COPY . .
RUN npm install
RUN npm run build 

# deploy stage 
FROM nginx:alpine
COPY --from=node /app/dist/* /usr/share/nginx/html


#####build & run #########
#docker build -t my-app:v1 .
#docker run -p 8080:8080 my-app:v1

### commit & push to dockerhub ###
#docker commit {docker ps} {docker userid}/{new Image name}:v1
#docker push {imagename}


###docker commands###
#docker ps -a
#docker images
#docker push
#docker pull
#docker commit


###add to the Conatainer Registry in gitlab###
#docker login registry.gitlab.com
#docker build -t registry.gitlab.com/mofvsv/ui .
#docker push registry.gitlab.com/mofvsv/ui